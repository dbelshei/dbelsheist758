---
title: "Homework 5"
author: "David Elsheimer"
date: "Due @ 11:59pm on October 28, 2019"
header-includes:
  - \usepackage{bm}
  - \newcommand{\Real}{\mathbb{R}}
  - \newcommand{\dom}{{\bf dom}\,}
  - \newcommand{\Tra}{^{\sf T}} 
  - \newcommand{\Inv}{^{-1}} 
  - \def\vec{\mathop{\rm vec}\nolimits}
  - \newcommand{\diag}{\mathop{\rm diag}\nolimits}
  - \newcommand{\tr}{\operatorname{tr}} 
  - \newcommand{\epi}{\operatorname{epi}}
  - \newcommand{\V}[1]{{\bm{\mathbf{\MakeLowercase{#1}}}}} 
  - \newcommand{\VE}[2]{\MakeLowercase{#1}_{#2}} 
  - \newcommand{\Vn}[2]{\V{#1}^{(#2)}} 
  - \newcommand{\Vtilde}[1]{{\bm{\tilde \mathbf{\MakeLowercase{#1}}}}}
  - \newcommand{\Vhat}[1]{{\bm{\hat \mathbf{\MakeLowercase{#1}}}}} 
  - \newcommand{\VtildeE}[2]{\tilde{\MakeLowercase{#1}}_{#2}} 
  - \newcommand{\M}[1]{{\bm{\mathbf{\MakeUppercase{#1}}}}}
  - \newcommand{\ME}[2]{\MakeLowercase{#1}_{#2}}
  - \newcommand{\Mtilde}[1]{{\bm{\tilde \mathbf{\MakeUppercase{#1}}}}}
  - \newcommand{\Mbar}[1]{{\bm{\bar \mathbf{\MakeUppercase{#1}}}}} 
  - \newcommand{\Mn}[2]{\M{#1}^{(#2)}} 
output: pdf_document
---

**Part 1.** In this assignment, you will derive and implement practical checks for assessing the numerical accuracy of the output of an algorithm for computing a constrained maximum likelihood estimator (MLE). Specifically, suppose we are estimating, via maximum likelihood, a parameter $\V{\beta} \in \mathcal{D} \subset \Real^p$ where $\mathcal{D}$ is compact.

$$
\Vhat{\beta} \in \underset{\V{\beta} \in \mathcal{D}}{\arg\min}\; \ell(\V{\beta}),
$$
where $\ell(\V{\beta})$ is the negative log-likelihood function. Suppose that $\ell(\V{\beta})$ is convex and differentiable.

1. Prove that there exists at least one $\V{\beta}^\star \in \mathcal{D}$ such that

$$
\ell(\V{\beta}^\star) = \underset{\V{\beta} \in \mathcal{D}}{\inf}\; \ell(\V{\beta}).
$$
In other words, the maximum likelihood estimation problem actually has a solution or MLE.

**Answer:**

This can be proven using Weierstrass. Note that $\ell(\V{\beta})$ is convex, differentiable, defined on a compact set, and thus is bounded below and attains its minimum. This minimum will be $\ell(\V{\beta}^*) =  \underset{\V{\beta} \in \mathcal{D}}{\inf}\; \ell(\V{\beta})$.

2. Suppose $\V{\beta}^\star$ is an MLE. Define the following function

$$
g(\V{\beta}) = \underset{\V{\theta} \in \mathcal{D}}{\max}\; \langle \nabla \ell(\V{\beta}), \V{\beta} - \V{\theta}\rangle.
$$
Use the fact that the first order Taylor approximation of a convex differentiable function $\ell(\V{\beta})$ is a global underestimator of $\ell(\V{\beta})$, to prove that

$$
g(\V{\beta}) \geq \ell(\V{\beta}) - \ell(\V{\beta}^\star).
$$

**Answer:**

Note that $g(\V{\beta}) = \underset{\V{\theta} \in \mathcal{D}}{\max}\; \langle \nabla \ell(\V{\beta}), \V{\beta} - \V{\theta}\rangle = \underset{\V{\theta} \in \mathcal{D}}{\max}\;(\nabla \ell(\V{\beta})\Tra(\V{\beta}-\V{\theta}))$

$$
\begin{aligned}
\ell(\V{\beta}^*) & \geq \ell(\V{\beta}) + \nabla\ell(\V{\beta})\Tra(\V{\beta}^*-\V{\beta})\\
& = \ell(\V{\beta}) - \nabla\ell(\V{\beta})\Tra(\V{\beta}-\V{\beta}^*)\\
\ell(\V{\beta}) - \ell(\V{\beta}^*) &\leq \ell(\V{\beta}) - (\ell(\V{\beta}) - \nabla\ell(\V{\beta})\Tra(\V{\beta}-\V{\beta}^*))\\
& =  \nabla\ell(\V{\beta})\Tra(\V{\beta}-\V{\beta}^*)\\
& \leq g(\V{\beta})
\end{aligned}
$$. 

3. What can you say about $\V{\beta}$ if $g(\V{\beta}) = 0$?

**Answer:**

$\ell(\V{\beta}) - \ell(\V{\beta}^*) \leq 0, \ell(\V{\beta}) \leq\ell(\V{\beta}^*)$, where $\V{\beta}^*$ is an arbitrary MLE. This means that $\V{\beta}$ is where the negative log-likelihood is minimized, or where the log-likelihood is maximized.

4. Consider the box constrained least squares problem. Let $\V{y} \in \Real^n$ be a response vector of $n$ observations and $\M{X} \in \Real^{n \times p}$ be a design matrix. Suppose we want to fit a linear regression model under box constraints, namely compute $\Vhat{\beta}$ such that

$$
\Vhat{\beta} \in \underset{\V{\beta} \in \mathcal{D}}{\arg\min}\; \frac{1}{2} \lVert \V{y} - \M{X}\V{\beta} \rVert_2^2,
$$
where
$\mathcal{D} = \{ \V{\beta} \in \Real^p : \VE{\beta}{j} \in [l_j, u_j], j = 1, \ldots, p\},$ for $l_j \leq u_j$ given. Derive a closed form expression for $g(\V{\beta})$ for the box constrained least squares problem.

**Answer:**

$$
\begin{aligned}
\ell(\V{\beta}) & = \frac{1}{2}\lVert\M{X}\V{\beta}-\V{y}\rVert_2^2\\
\nabla \ell(\V{\beta}) & = \M{X\Tra X}\V{\beta} - \M{X\Tra}\V{y}\\
g(\V{\beta}) & =\underset{\V{\theta} \in \mathcal{D}}{\max}\; \langle \nabla \ell(\V{\beta}), \V{\beta} - \V{\theta}\rangle\\
& = \underset{\V{\theta} \in \mathcal{D}}{\max}\; (\M{X\Tra X}\V{\beta}-\M{X\Tra}\V{y})\Tra(\V{\beta} - \V{\theta})\\
& =  \underset{\V{\theta} \in \mathcal{D}}{\max}\;\displaystyle\sum_{i=1}^n\nabla\ell(\V{\beta})_i(\V{\beta}_i-\V{\theta}_i)
\end{aligned}
$$

To maximimze this sum, the following rule must be applied to properly generate $\V{\theta}$. Where $\nabla\ell(\V{\beta})_i$ is negative, $\V{\theta}$ must be the upper bound. On the other hand, if $\nabla\ell(\V{\beta})_i$ is positive $\V{\theta}_i$ must be $l_i$.

5. What is the computational complexity of calculating $g(\V{\beta})$ for the box constrained least squares problem?

**Answer:**

Note that the computational complexity comes from $\nabla\ell(\V{\beta}) =  \M{X\Tra X}\V{\beta} - \M{X\Tra}\V{y}$. The complexity for $\M{X}\V{\beta}$ is $\mathcal{O}(np)$, and the same is true for $\M{X\Tra X}\V{\beta}$.  $\M{X\Tra}\V{y}$ also has complexity $\mathcal{O}(np)$. So the overall complexity of calculating $g(\V{\beta})$ is also $\mathcal{O}(np)$.

\newpage

**Part 2.** Evaluate the output of `L-BFGS-B` in the `optim` function

You will next add a wrapper function for computing box-constrained least squares and a suboptimality checker to your R package.

Please complete the following steps.

**Step 0:** Make an R package entitled "unityidST790".

**Step 1:** Write a function "boxls".

```{r, echo=TRUE}
#' Box Constrained Least Squares
#' 
#' \code{boxls} is a wrapper function around optim for doing box constrained
#' least squares estimation.
#' 
#' @param X Design matrix
#' @param y response
#' @param b initial value for regression coefficient vector
#' @param lb vector of lower bounds
#' @param ub vector of upper bounds
#' @param factr L-BFGS-B parameter controling tolerance for stopping
#' @param maxit L-BFGS-B parameter controling number of maximum iterations
#' @export
boxls <- function(X, y, b, lb, ub, factr=1e7, maxit=1e2) {
  
}
```
Your function should return $\Vhat{\beta}$ computed by calling `optim` using `b` as the intial parameter value, `method` = "L-BFGS-B", `lower = lb`, `upper = ub`, and `control = list(factr=factr, maxit=maxit)`. Read the help description of `optim` to see what `factr` and `maxit` do.

**Step 2:** Write a function "boxls_gap".

```{r, echo=TRUE}
#' Check Suboptimality for Box Constrained Least Squares
#'
#' \code{boxls_gap} computes the suboptimality of a regression coefficient vector.
#'
#' @param X Design matrix
#' @param y response
#' @param b putative value for regression coefficient vector
#' @param lb vector of lower bounds
#' @param ub vector of upper bounds
#' @export
boxls_gap <- function(X, y, b, lb, ub) {
  
}
```
Your function should return $g(\V{\beta})$.

\newpage

**Step 3:** Download `y.csv` and `X.csv`. Use your `boxls` function to compute the least squares estimator $\Vhat{\beta}$ subject to the constraints that $\beta_j \in [0, 1]$ for $j = 1, \ldots, 20$. Use your `boxls_gap` function to compute the suboptimality of the output of `boxls`. Try changing `factr` and `maxit` to see if you can improve the quality of your output from `boxls`. Describe your findings.

**Answer:**

```{r, echo=TRUE}
y<-as.matrix(read.csv("y.csv",header=FALSE))
X<- as.matrix(read.csv("X.csv",header=FALSE))


betas<- as.matrix(dbelsheiST758::boxls(X,y,runif(20),rep(0,20),rep(1,20),factr=1e-7,maxit=1e4)$par)
betas
dbelsheiST758::boxls_gap(X,y,betas,rep(0,20),rep(1,20))

betas<- as.matrix(dbelsheiST758::boxls(X,y,runif(20),rep(0,20),rep(1,20),factr=1e-7,maxit=1e1)$par)
dbelsheiST758::boxls_gap(X,y,betas,rep(0,20),rep(1,20))


betas<- as.matrix(dbelsheiST758::boxls(X,y,runif(20),rep(0,20),rep(1,20),factr=1e7,maxit=1e4)$par)
dbelsheiST758::boxls_gap(X,y,betas,rep(0,20),rep(1,20))

betas<- as.matrix(dbelsheiST758::boxls(X,y,runif(20),rep(0,20),rep(1,20),factr=1e7,maxit=1e1)$par)
dbelsheiST758::boxls_gap(X,y,betas,rep(0,20),rep(1,20))



betas<- as.matrix(dbelsheiST758::boxls(X,y,runif(20),rep(0,20),rep(1,20),factr=1e17,maxit=1e4)$par)
dbelsheiST758::boxls_gap(X,y,betas,rep(0,20),rep(1,20))

betas<- as.matrix(dbelsheiST758::boxls(X,y,runif(20),rep(0,20),rep(1,20),factr=1e17,maxit=1e1)$par)
dbelsheiST758::boxls_gap(X,y,betas,rep(0,20),rep(1,20))
```
The fewer iterations and the greater the factor, the less optimal our output becomes, as $g(\V{\beta})$ gets larger and larger when these are increased.


**Step 4:** Recall the first order optimality condition

$$
\langle \nabla \ell(\V{\beta}^\star), \V{\beta} - \V{\beta}^\star \rangle \geq 0 \quad\quad \text{for all $\V{\beta} \in \mathcal{D}$}.
$$
The left hand side is a directional derivative of $\ell$ evaluated at a global minimizer of $\ell$ pointing into the feasible set $\mathcal{D}$.

Generate 1000 replicates of random $\V{\beta}$s drawn from $\mathcal{D} = [0,1]^{20}$ and compute the directional derivative at $\Vhat{\beta}$, the output of `boxls`, using these 1000 $\V{\beta}$s. Use the `summary` function to report some quantiles of the directional derivatives. Do this for three different values of `factr`. Choose your values of `factr` to get $\Vhat{\beta}$ of varying degrees of suboptimality. Do the summary statistics make sense?


```{r, echo=TRUE}
library(Matrix)
b<-runif(20)
lb<-rep(0,20)
ub<-rep(1,20)

ddiv1<-Matrix(NA,nrow=0,ncol=1)
ddiv2<-Matrix(NA,nrow=0,ncol=1)
ddiv3<-Matrix(NA,nrow=0,ncol=1)
betamin1<-dbelsheiST758::boxls(X,y,b,lb,ub,factr=1e-7)$par
betamin2<-dbelsheiST758::boxls(X,y,b,lb,ub,factr=1e7)$par
betamin3<-dbelsheiST758::boxls(X,y,b,lb,ub,factr=1e17)$par

for(i in 1:1000){
  beta<-runif(20)
  ddiv1<-rbind(ddiv1,t(t(X)%*%(X%*%betamin1)-t(X)%*%y)%*%(beta-betamin1))
  ddiv2<-rbind(ddiv2,t(t(X)%*%(X%*%betamin2)-t(X)%*%y)%*%(beta-betamin2))
  ddiv3<-rbind(ddiv3,t(t(X)%*%(X%*%betamin3)-t(X)%*%y)%*%(beta-betamin3))
  }
summary(as.vector(ddiv1))
summary(as.vector(ddiv2))
summary(as.vector(ddiv3))

```
As more and more suboptimal $\Vhat{\beta}$'s are considered (where the factor is increased), the mean of the directional derivatives decreases, and the variance of the distribution increases, as noted by the increased spread of the distribution and more extreme maximum and minimum directional derivatives.
